
name := "hwo2014"

scalaVersion := "2.10.4"

resolvers += "sonatype-public" at "https://oss.sonatype.org/content/groups/public"

assemblySettings

jarName in assembly := "hwo2014.jar"

mainClass in assembly := Some("me.timnn.hwo2014.Main")

scalaSource in Compile := baseDirectory.value / ".." / "src"

unmanagedJars in Compile := (baseDirectory.value / ".." / "lib" ** ("*.jar" -- "*-javadoc.jar" -- "*-sources.jar")).classpath

outputPath in assembly := baseDirectory.value / (jarName in assembly).value

test in assembly := {}

