package me.timnn.hwo2014

import me.timnn.hwo2014.msg.{Throttle, Generic, Join}
import scala.annotation.tailrec

object Main extends App {
  val start = System.currentTimeMillis()

  val config = Config.create(args)
  val conn = new Connection(config)

  conn.send(Join(config.name, config.key))

  process()

  conn.close()

  println(s"Time: ${(System.currentTimeMillis() - start) / 1000} seconds")

  @tailrec
  private def process() {
    val msg = conn.receive()

    if (msg == null) return

    msg match {
      case g: Generic if g.typ == "carPositions" => conn.send(Throttle(.5))
      case _ => println(msg)
    }

    process()
  }
}
