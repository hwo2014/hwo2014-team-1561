package me.timnn.hwo2014.msg

trait Msg

trait Outgoing extends Msg

trait Incoming extends Msg
