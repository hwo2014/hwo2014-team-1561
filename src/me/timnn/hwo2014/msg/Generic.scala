package me.timnn.hwo2014.msg

class Generic(val typ: String, val data: String) extends Msg with Incoming {
  override def toString = {
    s"[$typ] $data"
  }
}
