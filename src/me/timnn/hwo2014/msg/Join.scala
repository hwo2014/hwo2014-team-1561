package me.timnn.hwo2014.msg

case class Join(name: String, key: String) extends Msg with Outgoing
