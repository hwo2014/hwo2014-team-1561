package me.timnn.hwo2014

import com.google.gson.GsonBuilder
import me.timnn.hwo2014.msg._
import me.timnn.hwo2014.gson.{ThrottleSerializer, JoinSerializer, MsgDeserializer}
import scala.reflect.ClassTag
import me.timnn.hwo2014.msg.Join

object MyGson {
  private val builder = new GsonBuilder()

  builder.disableHtmlEscaping()
  builder.serializeNulls()

  builder.registerTypeAdapter(classOf[Incoming], MsgDeserializer)

  builder.registerTypeAdapter(classOf[Join], JoinSerializer)
  builder.registerTypeAdapter(classOf[Throttle], ThrottleSerializer)

  private val gson = builder.create()
  builder.setPrettyPrinting()
  private val pretty = builder.create()

  def parse(msg: String): Incoming = {
    gson.fromJson(msg, classOf[Incoming])
  }

  def serialize[T <: Outgoing : ClassTag](msg: T) = {
    gson.toJson(msg)
  }
}
