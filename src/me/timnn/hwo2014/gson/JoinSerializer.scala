package me.timnn.hwo2014.gson

import com.google.gson.{JsonObject, JsonElement, JsonSerializationContext, JsonSerializer}
import me.timnn.hwo2014.msg.Join
import java.lang.reflect.Type

object JoinSerializer extends JsonSerializer[Join] {
  override def serialize(src: Join, typ: Type, ctx: JsonSerializationContext): JsonElement = {
    val msg = new JsonObject()
    msg.addProperty("msgType", "join")
    val data = new JsonObject()
    msg.add("data", data)
    data.addProperty("name", src.name)
    data.addProperty("key", src.key)
    msg
  }
}
