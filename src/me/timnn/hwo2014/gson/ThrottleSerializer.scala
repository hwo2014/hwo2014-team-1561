package me.timnn.hwo2014.gson

import com.google.gson.{JsonObject, JsonElement, JsonSerializationContext, JsonSerializer}
import me.timnn.hwo2014.msg.Throttle
import java.lang.reflect.Type

object ThrottleSerializer extends JsonSerializer[Throttle] {
  override def serialize(src: Throttle, typ: Type, ctx: JsonSerializationContext): JsonElement = {
    val msg = new JsonObject()
    msg.addProperty("msgType", "throttle")
    msg.addProperty("data", src.value)
    msg
  }
}
