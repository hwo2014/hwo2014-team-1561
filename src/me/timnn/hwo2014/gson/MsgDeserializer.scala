package me.timnn.hwo2014.gson

import com.google.gson._
import me.timnn.hwo2014.msg
import me.timnn.hwo2014.msg.Incoming
import java.lang.reflect.Type


object MsgDeserializer extends JsonDeserializer[Incoming] {
  override def deserialize(json: JsonElement, typeOfT: Type, ctx: JsonDeserializationContext): Incoming = {
    val obj = json.getAsJsonObject
    obj.get("msgType").getAsString match {
      case typ => new msg.Generic(typ, obj.get("data").toString)
    }
  }
}
