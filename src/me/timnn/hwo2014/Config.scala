package me.timnn.hwo2014

object Config {
  val DefaultHost = "testserver.helloworldopen.com"
  val DefaultPort = 8091
  val Name = "TimNN"
  val BotKey = "yOhjobOxc6lvuw"

  val Version = "v0.0.1"

  def create(args: Seq[String]): Config = {
    if (args.length >= 2) {
      Config(args(0), args(1).toInt, name(), BotKey)
    } else {
      Config(DefaultHost, DefaultPort, name(), BotKey)
    }
  }

  private def name() = {
    s"$Name - $Version".take(16)
  }
}

case class Config(host: String, port: Int, name: String, key: String)
