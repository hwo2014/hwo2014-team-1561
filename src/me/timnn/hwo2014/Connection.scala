package me.timnn.hwo2014

import java.net.Socket
import java.io._
import me.timnn.hwo2014.msg.{Outgoing, Msg}
import scala.reflect.ClassTag
import java.nio.file.{Files, Paths}
import java.text.SimpleDateFormat
import java.util.Date
import java.nio.file.StandardOpenOption._
import java.util.zip.{Deflater, GZIPOutputStream}
import Environment.DEBUG

object Connection {
  val LogDir = Paths.get("log").toAbsolutePath
  Files.createDirectories(LogDir)

  def logFile = LogDir.resolve(s"${new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date())}.log.gz")

  def logStream = new GZIPOutputStream(Files.newOutputStream(logFile, CREATE_NEW, WRITE)) {
    {
      `def`.setLevel(Deflater.BEST_COMPRESSION)
    }
  }
}

class Connection(val config: Config) {
  private val socket = new Socket(config.host, config.port)
  private val in = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  private val out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  private val log = if (DEBUG) new PrintStream(Connection.logStream, true, "UTF8") else null

  def receive(): Msg = {
    in.readLine() match {
      case null => if (DEBUG) log.println(">> null"); null
      case line => if (DEBUG) log.println(s">> $line"); MyGson.parse(line)
    }
  }

  def send[T <: Outgoing : ClassTag](msg: T) {
    val line = MyGson.serialize(msg)
    if (DEBUG) log.println(s"<< $line")
    out.println(line)
    out.flush()
  }

  def close() {
    socket.close()
    if (DEBUG) log.close()
  }
}
