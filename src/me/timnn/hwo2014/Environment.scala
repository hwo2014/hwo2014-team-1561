package me.timnn.hwo2014

import scala.collection.convert.wrapAsScala.mapAsScalaMap

object Environment {
  val IS_HWO = System.getenv().getOrElse("HWO_SYSTEM", "0").toInt == 1
  val DEBUG = !IS_HWO
}
